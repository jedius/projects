url = require("url")
module.exports = (env) ->
  config =
    port: 3000
    app:
      url: "http://localhost:3000"
      name: "rusrise"
      port: 3000
      secretString: "secretString"
      maxAge: 3 * 60 * 60 * 1000

    log:
      level: "info"
      path: __dirname + "/server.log"

    smtp:
      mailer:
        service: "Gmail"
        user: "rusrisetest@gmail.com"
        pass: "0o9i8u7y6t5r"

      templates:
        resetPassword:
          from: "admin"
          to: "user"
          subject: "Reset password on rusrise"
          text: "text"
          html: "<b>best regarts </b><br>rusrise developing team"

        confirmPassword:
          from: "admin"
          subject: "Confirm password on rusrise"
          text: "text"

    db:
      redisURL: url.parse(process.env.REDISTOGO_URL or "redis://localhost:6379")
      mongoUrl: process.env.MONGOHQ_URL or "mongodb://localhost/rusrise"
      public: __dirname+'/app/public/',
      images: 'pics/'

    validate:
      email: /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/
      string: /^([a-zA-Z]){1}([a-zA-Z0-9]){0,48}$/
      stringWithSpace: /^([a-zA-Z]){1}([ a-zA-Z0-9]){0,48}$/
      objectId: /^([a-z0-9]){24}$/
      password: /^[a-zA-Z'.\s]{6,25}$/
      url: /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/

  switch env
    when "production"
      config.port = process.env.PORT or 5000
      config.app.url = "http://rusrise.herokuapp.com"
      config.db.mongoUrl = process.env.MONGOHQ_URL or "mongodb://localhost/rusrise"
      break
    when "development"
      config.db.mongoUrl = process.env.MONGOHQ_URL or "mongodb://localhost/rusrise"
      break
    when "testing"
      config.db.mongoUrl = "mongodb://localhost/rusrise_testing"
      break
    else
      break
  config
