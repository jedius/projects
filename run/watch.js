var spawn = require('child_process').spawn;
var grunt = require('grunt');
var build = require('../app/server/build')
grunt.task.init = function() {};

grunt.registerTask('restart', function() {
	server.kill();
});

grunt.registerTask('build', function() {
	build.start(function(){console.log('builded');})
});

grunt.initConfig({
  watch: {
    server: {
    	files: [
    		'**/app/server/**'
		],
		tasks: ['restart']
    },
    client: {
    	files: [
    		'**/app/client/**',
    	],
    	tasks: ['build']
    },
    options: {
      nospawn: true,
    },
  },
});

grunt.loadNpmTasks('grunt-contrib-watch');

var server;
var start = function(){
	server = spawn('node',['run/dev.js'],{
		stdio: 'inherit'
	})
	server.on('exit',function(){
		start();
	})
}

start()
grunt.tasks(['watch']);