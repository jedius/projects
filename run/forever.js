var spawn = require('child_process').spawn
var fs = require('fs')
try {
  fs.mkdirSync(__dirname+'/../logs/');
} catch (e) {}


var start = function(){
  var date = new Date();
  var stream = fs.createWriteStream(__dirname+'/../logs/'+date.toString()+'.log')
  var process = spawn('node',[__dirname+'/production.js'])
  process.stdout.on('data',function(data){
    stream.write(data.toString())
  })
  process.stderr.on('data',function(data){
    stream.write(data.toString())
  })
  process.on('exit',function(){
    stream.end()
    start()
  })
}

start()