root = __dirname + "/../.."
fs = require("fs")
spawn = require("child_process").spawn
parallel = require("async").parallel
_ = require("underscore")

getTemplates = (cb) ->
  path = root + "/app/client/templates"
  fs.readdir path, (err, templates) ->
    result = {}
    templates_path = {}
    asyncArr = []
    i = 0
    while i < templates.length
      name = templates[i].split(".")[0]
      templates_path[name] = path + "/" + templates[i]
      i += 1
    readFile = (templates_path, t) ->
      asyncArr.push (done) ->
        fs.readFile templates_path[t], (err, data) ->
          result[t] = data.toString()
          done()

    for t of templates_path
      readFile templates_path, t  if templates_path[t] isnt 0
    parallel asyncArr, ->
      result = "window.Templates = " + JSON.stringify(result)
      fs.writeFile root + "/app/public/templates.js", result, (err) ->
        cb()  if cb

less = (cb) ->
  bin = root + "/node_modules/less/bin/lessc"
  src = root + "/app/client/stylesheets/main.less"
  dest = root + "/app/public/application.css"

  css = ""
  proc = spawn(bin, [src])
  proc.stdout.on "data", (data) ->
    css += data.toString()

  proc.stderr.on "data", (data) ->
    console.log data.toString()

  proc.on "exit", ->
    fs.writeFile dest, css, ->
      cb()  if cb


coffee = (cb) ->
  bin = root + "/node_modules/coffee-script/bin/coffee"
  src = root + "/app/client/app"
  options = [
    '-j'
    root + '/app/public/compiled.js'
    '-cb'
    src
  ]
  proc = spawn(bin, options)
  proc.stdout.on "data", (data) ->
    console.log(data.toString());

  proc.stderr.on "data", (data) ->
    console.log data.toString()

  proc.on "exit", ->
    cb()  if cb



uglify = (cb) ->
  path = root + "/app/client/lib"
  fs.readdir path, (err, libs) ->
    options = []
    # options.push "-b"
    # options.push "-nm"
    # options.push "-nmf"
    # options.push "-ns"
    libs = _.sortBy libs, (name)->
      return name;
    for lib in libs
      options.push path+'/'+lib
    options.push root + "/app/public/templates.js"
    options.push root + "/app/public/compiled.js"
    options.push "-o"
    options.push root + "/app/public/application.js"
    # console.log(options)
    bin_uglify = root + "/node_modules/uglify-js/bin/uglifyjs"
    proc = spawn(bin_uglify, options)
    proc.stdout.on "data", (data) ->
      console.log data.toString()

    proc.stderr.on "data", (data) ->
      console.log data.toString()

    proc.on "exit", ->
      fs.unlink root + "/app/public/templates.js", (err) ->
        fs.unlink root + "/app/public/compiled.js", (err) ->
          cb()  if cb

module.exports.start = (cb) ->
  getTemplates ->
    coffee ->
      uglify ->
        less ->
          cb()  if cb
