utils = {}
utils.crypt = (str) ->
  cipher = undefined
  crypted = undefined
  cipher = glob.modules.crypto.createCipher("aes-256-cbc", glob.config.app.secretString)
  crypted = cipher.update(str, "utf8", "hex")
  crypted += cipher.final("hex")

utils.decrypt = (str) ->
  decipher = undefined
  decrypted = undefined
  try
    decipher = glob.modules.crypto.createDecipher("aes-256-cbc", glob.config.app.secretString)
    decrypted = decipher.update(str, "hex", "utf8")
    return decrypted += decipher.final("utf8")
  catch err
    log.warn err, "utils.decrypt"
    return false

utils.randomString = (length) ->
  chars = undefined
  i = undefined
  str = undefined
  chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz".split("")
  length = Math.floor(Math.random() * chars.length)  unless length
  str = ""
  i = 0
  while i < length
    str += chars[Math.floor(Math.random() * chars.length)]
    i++
  str

utils.generateNumericString = (length) ->
  chars = undefined
  i = undefined
  str = undefined
  chars = "0123456789".split("")
  length = Math.floor(Math.random() * chars.length)  unless length
  str = ""
  i = 0
  while i < length
    str += chars[Math.floor(Math.random() * chars.length)]
    i++
  str

utils.generateToken = ->
  chars = "_!abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
  token = utils.randomString(6) + new Date().getTime()
  x = 0

  while x < 16
    i = Math.floor(Math.random() * 62)
    token += chars.charAt(i)
    x++
  token

module.exports = utils