logFrom = "Auth passport"
passport = glob.modules.passport
LocalStrategy = glob.modules.passportLocal.Strategy

passport.serializeUser (user, done) ->
  done null, user  if user

passport.deserializeUser (user, done) ->
  if user
    mongo.user.findById user.id, (err, user) ->
      unless err
        if user
          done null, user
        else
          log.warn logFrom, "user not found"
          done null, false
      else
        console.log "error: ", err
        done err, false

  else
    log.info logFrom, "id not found"

passport.use "local", new LocalStrategy({
    usernameField: "email"
    passwordField: "password"
  },(email, password, done) ->
    # log.info logFrom, "login strategy"
    if glob.config.validate.email.test(email)
      if glob.config.validate.password.test(password)
        password = glob.modules.crypto.createHash('md5').update(password).digest("hex")
        mongo.user.findOne
          email: email
        , (err, user) ->
          unless err
            if user
              if user.password is password
                done null, user.entity
                # if user.confirmation is true
                #   log.info logFrom, "success"
                #   done null, user.entity
                # else
                #   log.warn logFrom, "account not confirmed"
                #   done null, false, "account not confirmed"
              else
                done null, false, "wrong password"
            else
              log.warn logFrom, "email does not exist or query err"
              done null, false, "email does not exist"
          else
            log.warn logFrom, "user find error"
            done err, false, "wrong login or password"

      else
        log.warn logFrom, "invalid password"
        done null, false, "invalid password"

    else
      log.warn logFrom, "invalid email"
      done null, false, "invalid email"

)

passport.use "local-join", new LocalStrategy({
    usernameField: "email"
    passwordField: "password"
  },(email, password, done) ->
    if glob.config.validate.email.test(email)
      if glob.config.validate.password.test(password)
        mongo.user.findOne
          email: email
        , (err, user) ->
          unless err
            unless user
              nUser = {}
              nUser.role = "user"
              nUser.email = email
              password = glob.modules.crypto.createHash('md5').update(password).digest("hex")
              nUser.password = password
              nUser.dt = new Date
              nUser.lm = new Date
              nUser.confirmation = false
              mongo.user.create nUser, (err, user) ->
                unless err
                  if user
                    log.info logFrom, "success"
                    done null, user.entity
                  else
                    log.error logFrom, "error: ", err
                    done err, false, err

                else
                  log.error logFrom, "error: ", err
                  done err, false, "database error"

            else
              done null, false, "user already registred"

          else
            done err, false, "database error"

      else
        done null, false, "invalid password"

    else
      done null, false, "invalid email"

    # else
    #   done null, false,
    #     error: "bad credentials"

)