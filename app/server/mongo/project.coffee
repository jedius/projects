mongoose = glob.modules.mongoose
schema = new mongoose.Schema

    userId:
        type: "string"

    email:
        type: "string"

    projectName:
        type: "string"

    projectUrl:
        type: "string"

    projectDescription:
        type: "string"

    imageCount:
        type: "number"

module.exports = mongoose.model("project", schema)
