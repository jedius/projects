mongoose = glob.modules.mongoose
member = new mongoose.Schema
  name:
    type: "string"

  surname:
    type: "string"

  birthYear:
    type: "number"

  country:
    type: "string"

  region:
    type: "string"

  description:
    type: "string"

  profession:
    type: "string"

  experience:
    type: "string"

  picture:
    type: "string"

member.virtual("entity").get ->
  entity =
    _id: @_id.toString()
    name: @name
    surname: @surname
    birthYear: @birthYear
    country: @country
    region: @region
    description: @description
    experience: @experience
    profession: @profession
    picture: @picture

module.exports = mongoose.model("member", member)
