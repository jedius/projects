module.exports = (app)->
    multipart = require("multipart")
    http = glob.modules.http
    fs = glob.modules.fs
    exec = require("child_process").exec
    spawn = require("child_process").spawn
    app.post '/storage/upload', (req, res) ->
      console.log "upload start"
      try
        file = JSON.parse(req.body.file)
        path = glob.config.db.images + Date.now() + file.name
        start = req.body.data.indexOf(",") + 1
        base64 = req.body.data.substring(start, req.body.data.length)
        data = new Buffer(base64, "base64")
        fs.writeFile glob.config.db.public + path, data, (err) ->
          if err
            console.log err
            res.send err: err
          else
            res.send url: "/" + path

      catch e
        console.log e
        res.send 500
