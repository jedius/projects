fs = require "fs"

module.exports = (app)->
  app.get '/projects', (req,res) ->
    console.log('PROJECT');
    console.log req.query.page
    if req.query.page
      req.body.skip = (req.query.page - 1) * 8
      req.body.limit = 8
    else
      req.body.skip = 0
      req.body.limit = 8
    mongo.project.count {}, (err, count) ->
      console.log('COUNT', count);
      mongo.project.find().skip(req.body.skip).limit(req.body.limit).exec (err,projects)->
        if err
          console.log(err);
        else
          res.send projects

  # create project
  app.post '/projects', (req,res) ->
      project = new mongo.project(req.body)
      if req.user
        project.userId = req.user.id
        project.email = req.user.email
        project.imageCount = req.body.projectImg.length
      project.save (err)->
        console.log(err) if err
        #console.log 'require!!!',req.body 
        if req.body.projectImg isnt ""     
            console.log '!!!!',req.body.projectImg
            start = req.body.projectImg.indexOf(',')+1
            base64 = req.body.projectImg.substring start,req.body.projectImg.length
            data = new Buffer(base64, 'base64')
            fs.writeFile 'app/public/images/'+project._id+'.jpg',data, (err)->
                if err
                    console.log err
        res.send project

  app.get '/projectsCount', (req, res) ->
    console.log "projects, get count"
    mongo.project.count {}, (err, count) ->
      console.log count
      res.send count: count


  app.get '/projects/:id', (req,res) ->
    console.log('get project');
    mongo.project.findById req.params.id, (err,project)->
      if err
        console.log(err);
        res.send 400
      if project
        res.send project
      else
        res.send 400

  app.put '/projects/:id', (req,res) ->
    console.log('update project');
    mongo.project.update {_id: req.params.id}, req.body, {}, (err,n)->
      if err
        console.log(err);
      console.log(n);
      res.send 200

  app.delete '/projects/:id', (req,res) ->
    console.log('delete project');
    mongo.project.findById req.params.id, (err,project)->
      if err
        console.log(err);
        res.send 400
      if project
        fs.unlink 'app/public/images/'+req.params.id+'.jpg', (err)->
            throw err if err
        project.remove (err)->
          if err
            console.log(err);
          res.send 200
      else
        res.send 400
