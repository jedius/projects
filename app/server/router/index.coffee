module.exports = (app) ->

	app.get "/", (req, res) ->
	  id = ''
	  email = ''
	  if req.isAuthenticated() and req.user
	  	id = req.user.id
	  	email = req.user.email
	  res.render "index.jade",
	    title: "rusrise"
	    id: id
	    email: email


	require("./auth") app
	require("./project") app
	require("./member") app
	require("./storage") app
	require("./actions") app
