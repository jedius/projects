passport = glob.modules.passport
logFrom = "auth router: "
module.exports = (app) ->
  app.post "/login", (req, res) ->
    passport.authenticate("local", (err, user, info) ->
      unless err
        if user
          req.logIn user, (err) ->
            if err
              res.send
                code: 500
                message: err
              next err
            else
              res.send
                code: 200
                user: user
        else
          res.send
            code: 400
            error: "wrong login or password"

      else
        console.log('err',err);
        res.send
          code: 400
          error: "login error"

    )(req, res)

  # mail = glob.config.smtp.templates.confirmPassword
  # mail.to = user.email
  # mail.html = "Welcome to Rusrise site. For complete the registration you need to login by using One Time Password: <h2>" + user.otp + "</h2><a href=" + acceptLink + ">HERE</a><br><br>If this message is sent to you by mistake, click on this link <a href=" + denyLink + ">here</a>, or ignore this message.<br><br><br> With love Rusrise site developing team"
  # smtpTransport.sendMail mail, (error, response) ->

  app.post("/signup", (req, res, next) ->
    if req.body.password is req.body.confirmPassword
      passport.authenticate("local-join", (err, user, info) ->
        unless err
          if user
            cryptedId = glob.utils.crypt(user.id)
            acceptLink = glob.config.app.url + "/confirmEmail/accept?otp=" + cryptedId
            denyLink = glob.config.app.url + "/confirmEmail/deny?otp=" + glob.utils.crypt(user.id)
            mail = glob.config.smtp.templates.confirmPassword
            mail.to = user.email
            mail.html = "Welcome to Rusrise site. To activate your profile you need to follow this : <a href=" + acceptLink + ">link</a><br><br>If this message is sent to you by mistake ignore this message.<br><br><br> With love Rusrise site developing team"
            global.glob.smtpTransport.sendMail mail, (error, response) ->
              if error
                log.error logFrom, err
              else
                res.send
                  code: 200
                  data: user

          else
            log.warn logFrom, info
            res.send
              code: 400
              error: info

        else
          res.send
            code: 400
            error: "authenticate error"

      ) req, res, next

    else
      log.warn logFrom, "password and confirm password do not match"
      res.send
        code: 400
        error: "password and confirm password do not match"
  )

  app.get("/confirmEmail/:userId", (req, res, next) ->
    confirmationId = glob.utils.decrypt(req.query.otp)
    if req.params.userId is "deny"
      mongo.user.remove
        _id: confirmationId
      , (err, ok) ->
        unless err
          if ok
            req.logout()
            res.redirect "/"
          else
            log.error logFrom, "not removed"
            res.send
              code: 400
              error: "not removed"

        else
          log.error logFrom, err
          res.send
            code: 400
            error: "user remove error"


    if req.params.userId is "accept"
      #find user
      mongo.user.findById confirmationId, (err, user) ->
        unless err
          if user
            if user.confirmation is false
              user.confirmation = true
              user.save (err) ->
                unless err 
                  #find new user mates
                  mongo.house.findOne
                    _id: user.house
                  , (err, house) ->
                    unless err
                      if house
                        mates = []
                        i = 0

                        while i < house.flatmatesId.length
                          mates.push house.flatmatesId[i]  unless house.flatmatesId[i] is user._id
                          i++
  
                        #send notification to just created user's mates 
                        mongo.notification.create
                          sharedUsersId: mates
                          dateNotify: Date.now()
                          message: "New flatmate " + user.name + " successfully join to your house"
                        , (err, notification) ->
                          unless err
                            if notification
                              log.info logFrom, "notification created", notification
                              res.redirect "/#auth"
                            else
                              log.error logFrom, "notification not created"
                              res.send
                                code: 400
                                error: "notification not created"

                          else
                            log.error logFrom, err
                            res.send
                              code: 400
                              error: "notification create error"

                      else
                        log.warn logFrom, "house not found"
                        res.redirect "/#auth"
                    else
                      log.error logFrom, err
                      res.send
                        code: 400
                        error: "house find error"

                else
                  res.send
                    code: 400
                    error: "user save error"

            else
              log.warn logFrom, "account already confirmed"
              res.redirect "/#auth"
          else
            log.warn logFrom, "user not found"
            res.send
              code: 400
              error: "user not found"

        else
          log.warn logFrom, "user find error"
          res.send
            code: 400
            error: "user find error"

    else
      log.warn logFrom, "req.params not found"
      res.send
        code: 400
        error: "req.params not found"

  )

  app.get "/logout", (req, res) ->
    console.log('logout');
    req.logout ->
      console.log('logout callback?');
    res.redirect "/"
