getProjectEmail = (projectId,cb)->
  mongo.project.findById projectId, (err,project)->
    if err
      console.log(err)
      cb err
      return
    if !project.email
      console.log('no project email')
      cb 'no project email'
      return
    cb null,project.email

module.exports = (app)->
  
  # send message
  app.post '/message/:projectId', (req,res)->
    getProjectEmail req.params.projectId, (err,email)->
      if !err and email
        if req.user
          id = req.user.id
        else
          id = 'anonymous'
        item = new mongo.message
          userId: id
          projectId: req.params.projectId
          message: req.body.message
        
        mail =
          from: glob.config.smtp.mailer.user
          to: email
          subject: 'message'
          text: req.body.message
        glob.email mail, (err,res)->
          if err
            console.log('sendMail err:',err)
            console.log('res:',res);
        item.save (err,item)->
          res.send
            code: 200
            data: item
      else
        if err
          console.log(err);
        res.send
          code: 400
          message: err or 'no email'

  # send fund
  app.post '/fund/:projectId', (req,res)->
    getProjectEmail req.params.projectId, (err,email)->
      if !err and email
        mail =
          from: glob.config.smtp.mailer.user
          to: email
          subject: 'fund'
          text: req.body.os1
        glob.email mail, (err,res)->
          if err
            console.log('sendMail err:',err)
            console.log('res:',res);

      for key,value of req.body
        if value is req.body.os0
          index = parseInt(key.replace('option_select',''))
          amount = req.body['option_amount'+index]
      if req.user
        id = req.user.id
      else
        id = 'anonymous'
      item = new mongo.fund
        userId: id
        projectId: req.params.projectId
        message: req.body.os1
        amount: amount
      item.save (err,item)->
        if err
          console.log(err);
        res.send
          code: 200
          data: item

  # send invite
  app.post '/invite/:projectId', (req,res)->
    if req.user
      id = req.user.id
    else
      id = 'anonymous'
    unless req.body.message
      req.body.message = 'invite to #{req.params.projectId}'
    item = new mongo.invite
      userId: id
      projectId: req.params.projectId
      message: req.body.message
      emails: req.body.email

    if req.body.email[0]
      mail =
        from: glob.config.smtp.mailer.user
        to: req.body.email
        subject: 'invite'
        text: req.body.message
      glob.email mail, (err,res)->
        if err
          console.log('sendMail err:',err)
          console.log('res:',res);
    
    item.save (err,item)->
      if err
        console.log(err);
      res.send
        code: 200
        data: item

