module.exports = (app)->
    app.get '/memberlist', (req, res) ->
      console.log req.query.page
      if req.query.page
        req.body.skip = (req.query.page - 1) * 6
        req.body.limit = 6
      else
        req.body.skip = 0
        req.body.limit = 6
      mongo.member.count {}, (err, count) ->
        mongo.member.find({}).skip(req.body.skip).limit(req.body.limit).exec (err, members) ->
          unless err
            res.send members
          else
            log.info logFrom, err
            res.send
              status: 500
              error: err

    
    app.get '/members/p/:page', (req, res) ->
      console.log req.query.page
      if req.query.page
        req.body.skip = (req.query.page - 1) * 8
        req.body.limit = 8
      else
        req.body.skip = 0
        req.body.limit = 8
      mongo.member.count {}, (err, count) ->
        mongo.member.find({}).skip(req.body.skip).limit(req.body.limit).exec (err, members) ->
          unless err
            res.send members
          else
            log.info logFrom, err
            res.send
              status: 500
              error: err



    app.post '/members', (req, res) ->
      #delete req.body._id
      console.log 'creat model'
      mongo.member.create req.body, (err, member) ->
        unless err
          console.log "member", member.entity, member
          res.send member.entity  if member
        else
          res.send
            code: 400
            error: "member create error"



    app.get '/members/:id', (req, res) ->
      id = req.params.id
      console.log "Retrieving member: " + id
      mongo.member.findOne
        _id: id
      , (err, item) ->
        console.log "item", item
        res.send item


    app.put '/members/:id', (req, res) ->
      console.log "update"
      id = req.params.id
      delete req.body._id

      mongo.member.update
        _id: req.params.id
      , req.body, (err, count, info) ->
        res.send()


    app.get '/membersCount', (req, res) ->
      console.log "members, get count"
      mongo.member.count {}, (err, count) ->
        console.log count
        res.send count: count


    app.delete '/members/:id', (req, res) ->
        id = req.params.id
        mongo.member.remove {_id: req.params.id}, (err) ->
            if !err
                console.log(err)
                res.send
                    code:200
            else
                res.send
                    err: err


