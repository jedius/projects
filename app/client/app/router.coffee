Router = Backbone.Router.extend
  routes:
    "projects":           "projects"
    "projects/page/:page":"projects"
    "members":            "membersList"
    "members/page/:page": "membersList",
    "members/add":        "addMembers"
    "members/:id":        "memberDetails",
    "projects/:id":       "project"
    "post":               "post"
    "post/:id":           "postPreset"
    "news":               "news"
    "services":           "services"
    "home":               "home"
    "contacts":           "contacts"
    "login":              "login"
    "dialog":             "dialog", 
    "*actions":           "defaultAction"

  initialize: ->
    @projectsCollection = new ProjectCollection
    @views = 
      project: []

  start: ->
    Backbone.history.start()
 
  post: ->
    unless @views.post
      @views.post = new PostView
        el: $('<div id=post class=appModule />').appendTo($('#body'))
        collection: @projectsCollection
    @views.post.render()

  postPreset: (id)->
    unless @views.post
      @views.post = new PostView
        el: $('<div id=post class=appModule />').appendTo($('#body'))
        collection: @projectsCollection
    @views.post.render(id)

  project: (id)->
    unless @views.project[id]
      model = new ProjectModel
        _id: id
      @views.project[id] = new ProjectView
        el: $("<div id=project_#{id} class=appModule />").appendTo($('#body'))
        model: model          
    @views.project[id].render()
  
  news: ->
    unless @views.news
      @views.news = new NewsView
        el: $('<div id=about class=appModule />').appendTo($('#body'))
        collection: new NewsCollection
    @views.news.render()
  
  services: ->
    unless @views.services
      @views.services = new ServicesView
        el: $('<div id=services class=appModule />').appendTo($('#body'))
        collection: new ServicesCollection
    @views.services.render()    

  addMembers: ->
    @views.memberDetail = new MemberView
      el: $('<div id=content class=appModule />').appendTo($('#body')) 
      model: new Member
    @views.memberDetail.render()    
    #@headerView.selectMenuItem "addmembers-menu"

  contacts: ->
    unless @views.contacts
      @views.contacts = new ContactsView
        el: $('<div id=contacts class=appModule />').appendTo($('#body')) 
        model: new ContactModel
    @views.contacts.render()
  
  projects: ->
    unless @views.projects
      @views.projects = new ProjectsView
        el: $('<div id=projects-portfolio class=appModule />').appendTo($('#body'))
        collection: @projectsCollection
    @views.projects.render()
    
  membersList: (page)->
    console.log('page!!',page);
    self = this
    p = (if page then parseInt(page, 10) else 1)
    #console.log '@views.memberList', @views.memberList
    @views.memberList = new MemberCollection()  unless @views.memberList
    console.log('!!!',@views.memberList);
    @views.memberList.getCount (count)=>
      console.log 'getCount go'
      @views.memberList.fetch
        data: {page: p}
        success: =>
          console.log 'success'
          @views.memberListView = new MemberListView
              collection: @views.memberList
              page: p
              count: count
              el: $('<div id=memberListView class=appModule />').appendTo($('#body'))
          @views.memberListView.render()

  memberDetails: (id) ->
    member = new Member(_id: id)
    console.log "member ", member
    member.fetch success: (lol) =>
        @views.memberDetail = new MemberView
          el: $('<div id=content class=appModule />').appendTo($('#body')) 
          model: member
        @views.memberDetail.render()    


  home: ->
    unless @views.home
      @views.home = new HomeView
        el:$('<div id=home class=appModule />').appendTo($('#body'))
    @views.home.render()

  login: ->
    unless @views.login
      @views.login = new LoginView
        el: $('<div id=login class=appModule />').appendTo($('#body'))
    @views.login.render()

  dialog: ->
    unless @views.dialog
      @views.dialog = new DialogView
        el: $('<div id=dialog class=appModule />').appendTo($('#body'))
    @views.dialog.render()

  defaultAction: ->
    console.log 'defaultAction'
    @navigate 'home', trigger: true

