HomeView = Backbone.View.extend

  initialize: ->
    console.log 'home view'

  render: ->
    @template = _.template window.Templates.home
    @$el.html @template
    $('.appModule').hide()
    @$el.show()

