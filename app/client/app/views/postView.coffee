PostView = Backbone.View.extend

    events:
        'submit .projectForm': 'createProject'
        'change #forFile': 'displayFile'
        #'change #forFiles': 'displayFile'
        'click .cancel_button': 'cancel'

  
    initialize: ->
        console.log 'postView'

    render: (id)->
        @template = _.template(window.Templates.post)
        @$el.html @template
        $('.appModule').hide();
        @$el.show();

    createProject: (e)->
        e.preventDefault()
        self = @
        input = document.getElementById('forFile')
        console.log '!!!',input 
        fr = new FileReader()
        fr.onload = (e)->
            console.log '!!!!', e.target.result 
            data = $('.projectForm').serializeObject()
            data.projectImg = e.target.result
            console.log 'submit',data
            self.collection.create data,
                success: (model)->
                    if model
                        id = model.get('_id')
                        window.app.router.navigate("projects", trigger: true)
        fr.readAsDataURL input.files[0]

        return false

    displayFile:(e)->
        imgList = $('ul#img-list')
        for file in e.target.files
            li = $('<li class="preview" style="float:left; margin:10px"/>').appendTo(imgList);
            $('<div/>').text(file.name).appendTo(li);
            img = $('<img/>').appendTo(li);
            li.get(0).file = file;
            reader = new FileReader();
            reader.onload = ((aImg)-> 
                (e)->
                    aImg.attr('src', e.target.result);
                    aImg.attr('width', 150);
            )(img);
            
            reader.readAsDataURL file
    cancel:->
        $('.preview').remove()
