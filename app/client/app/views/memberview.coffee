window.MemberView = Backbone.View.extend(

  events:
    #change: "change"
    "click .save": "save"
    "click .delete": "deleteMember"
    "drop #picture": "dropHandler"
    "dragover #picture": "dragoverHandler"

  initialize: ->
    console.log 'initialize MemberView'
    #@template = 
    #@render()

  render: ->
    #model = @model.toJSON()
    #console.log model
    @template = _.template window.Templates.memberDetail
    @$el.html @template({member:@model.toJSON(), user:window.app.user.toJSON()})
    $('.appModule').hide();
    @$el.show();
    #model.user = window.user.get("firstName")
    #console.log "model", model
    #$(@el).html @template(model)

  change: (event) ->
    
    # Remove any existing alert message
    utils.hideAlert()
    
    # Apply the change to the model
    target = event.target
    change = {}
    change[target.name] = target.value
    @model.set change
    
    # Run validation rule (if any) on changed item
    check = @model.validateItem(target.id)
    if check.isValid is false
      utils.addValidationError target.id, check.message
    else
      utils.removeValidationError target.id

  save: (e) ->
    self = this
    #check = @model.validateAll()
    #if check.isValid is false
      #utils.displayValidationErrors check.messages
      #return false
    data = @$('.member-form').serializeObject()
    console.log 'data', data
    #@model.set data
    console.log "before save"
    console.log "member", @model
    @model.save data,
      success: (model) ->
        console.log "success"
        window.app.router.navigate "members", true

      error: ->
        console.log "error"

    e.preventDefault()
    false

  deleteMember: ->
    @model.destroy success: ->
      alert "Member deleted successfully"
      window.history.back()

    false

  dropHandler: (event) ->
    
    # console.log('dropHandler');
    self = this
    event.stopPropagation()
    event.preventDefault()
    e = event.originalEvent
    e.dataTransfer.dropEffect = "copy"
    @pictureFile = e.dataTransfer.files[0]
    reader = new FileReader()
    reader.onloadend = ->
      
      # console.log(reader);
      self.$(".picture").attr "src", reader.result
      console.log "result", reader.result
      
      # console.log('going to upload');
      self.uploadFile reader.result, self.pictureFile

    reader.readAsDataURL @pictureFile

  dragoverHandler: (event) ->
    event.preventDefault()


  uploadFile: (blob, file) ->
      self = this
      if file.type is "image/jpeg" or file.type is "image/png"
        progressHandlingFunction = ->
        $.ajax
          url: "/storage/upload"
          type: "POST"
          data:
            data: blob
            file: JSON.stringify(file)

          success: (res) ->
            console.log "res", res
            if res and res.url
              self.$("#picture").attr "src", res.url
              self.model.save picture: res.url

)
