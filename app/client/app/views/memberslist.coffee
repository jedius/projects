window.MemberListView = Backbone.View.extend(
  initialize: ->
    #@render()

  render: ->
    console.log "coll", @collection
    self = this
    startPos = (@options.page - 1) * 10
    endPos = Math.min(startPos + 10, @collection.length)
    count = Math.ceil(@options.count / 10)
    $(@el).html "<ul class=\"thumbnails\"></ul>"
    #@$el.html @template({member:@model.toJSON()})
    $('.appModule').hide();
    @$el.show();

    #@collection.fetch
    @collection.each (model, i) ->
      console.log model, i
      # model.set('picture', 'default.jpg')
      $(".thumbnails", self.el).append new MemberListItemView(model: model).render().el

    
    # $('.thumbnails', this.el).append(new LocationListItemView({model: locations[i]}).render().el);
    # $(@el).append new Paginator(
    #   collection: @collection
    #   page: @options.page
    #   count: count
    #   entity: "members"
    # ).render().el
    #imgs = @$('.thumbnail img')
    #for img in imgs
        ##console.log '@$(imgs[i])', @$(img).width(125), @$(img).height()
        #if(@$(img).width() > @$(img).height())
           #@$(img).width('125px') 
        #else
           #@$(img).height('125px') 
)

window.MemberListItemView = Backbone.View.extend(
  tagName: "li"
  initialize: ->
    @template = _.template window.Templates.MemberListItemView
    @model.bind "change", @render, this
    @model.bind "destroy", @close, this

  render: ->
    console.log @model.toJSON()
    $(@el).html @template( model: @model.toJSON() )
    this
)
