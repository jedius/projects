ServicesView = Backbone.View.extend

  initialize: ->
    console.log 'services view'

  render: ->
    @template = _.template window.Templates.services

    @$el.html @template
    $('.appModule').hide();
    @$el.show();

