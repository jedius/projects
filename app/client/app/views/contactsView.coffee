ContactsView = Backbone.View.extend

  initialize: ->
    console.log 'news view'

  render: ->
    @template = _.template window.Templates.contacts
      
    @$el.html @template
      user:app.user.toJSON()
    $('.appModule').hide();
    @$el.show();
    @map()

  map: (addr) ->
    console.log 'map'      
    init = ->
      geocoder = new ymaps.geocode('таганрог Щаденко 69', 
        results: 1)
      geocoder.then (res) ->
        coord = res.geoObjects.get(0).geometry.getCoordinates()
        map = new ymaps.Map("map",
            center: coord
            zoom: 7
            behaviors: ["default", "scrollZoom"]
            #controls: ["mapTools"]
            )
        map.geoObjects.add res.geoObjects.get(0)
        map.zoomRange.get(coord).then (range) ->
            map.setCenter coord, range[1] - 1


        map.controls.add("mapTools").add("zoomControl").add "typeSelector"

    ymaps.ready init

