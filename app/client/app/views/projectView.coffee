ProjectView = Backbone.View.extend
    events:
        'click .delproj': 'deleteProject'

    initialize: ->
        console.log 'projeCT view'

    render: ->
        @model.fetch
            success: =>
                @$el.html _.template window.Templates.project,
                    project:@model.toJSON(),
                    user:app.user.toJSON()
            $('.appModule').hide();
            @$el.show();
  
    deleteProject:(e)->
        id =e.currentTarget.id
        @model.deleteProject id,()->
            window.app.router.navigate("projects/", trigger: true)

