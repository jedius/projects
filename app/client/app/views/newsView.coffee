NewsView = Backbone.View.extend

  initialize: ->
    console.log 'news view'

  render: ->
    @template = _.template window.Templates.news

    @$el.html @template
    $('.appModule').hide();
    @$el.show();

