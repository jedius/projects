DialogView = Backbone.View.extend
  events:
    'click .project': 'openProject'

  initialize: ->
    console.log ('Projects view')

  render: ()->
    @template = _.template window.Templates.dialog
    @$el.html @template
    $('.appModule').hide()
    @$el.show()


  openProject: (e)->
    id = $(e.currentTarget).attr('_id')
    window.app.router.navigate("projects/#{id}", trigger: true)

