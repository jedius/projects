ProjectsView = Backbone.View.extend
  
  events:
    'mouseenter .project': 'openProject'
    'mouseleave .project': 'closeProject'
    'click .icon-trash': 'deleteProject'

  initialize: ->
    console.log ('Projects view')

  render: ()->
    self = @
    startPos = (@options.page - 1) * 10
    endPos = Math.min(startPos + 10, @collection.length)
    count = Math.ceil(@options.count / 10)
    console.log('USER',app.user.toJSON());
    @collection.fetch
      success:=>
        @$el.html _.template(window.Templates.projects, projects: @collection.toJSON(), user: app.user.toJSON())
        $('.appModule').hide();
        console.log('!!!coll',@options);
        # $(@el).append new Paginator(
        #   collection: @collection
        #   page: @options.page
        #   count: count
        #   entity: "project"
        # ).render().el
        @$el.show();
        if window.app.user.get('id')
          @$('.create-project-button').show()
        window.app.user.on 'change', =>
          @$('.create-project-button').show()

  openProject: (e)->
    $(e.currentTarget).find('.description-window').slideDown()
    #window.app.router.navigate("projects/#{id}", trigger: true)


  closeProject: (e)->
    $('.description-window').slideUp()

  deleteProject:(e)->
    id = $(e.currentTarget).attr('id')
    window.app.router.navigate("projects/#{id}", trigger: true)