LoginView = Backbone.View.extend
	events: 
        "submit .signUpForm":"signUp"
        "submit .loginForm": "login"
        "click .showLogin":"showLogin"
        "click .showPassword":"showPassword"

    initialize: ->

    render: ->
        @template = _.template(window.Templates.login)
        @$el.html @template
        $('.appModule').hide();
        @$el.show();
        @$('.signUp').hide()


    login: (e)->
        data = @$('.loginForm').serializeObject()
        console.log data
        app.user.login data, (res)->
            if res.code is 200
                window.app.user.set(res.user)
                app.router.navigate('/projects', trigger: true)
            else
                console.log(res.code)
                console.log(res)
        e.preventDefault()
    
    signUp:(e)->
        data = @$('.signUpForm').serializeObject()
        e.preventDefault()
        app.user.signUp data, (res)->
            if res.code is 200
                window.app.user.set(res.user)
                app.router.navigate('/projects', trigger: true)
                console.log res
                console.log 'signup success'
            else
                console.log res
        e.preventDefault()

    showLogin: ->
        @$('.signUp').hide()
        @$('.login_main').show()

    showPassword: ->
        @$('.login_main').hide()
        @$('.signUp').show()
