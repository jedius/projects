ProjectModel = Backbone.Model.extend
    url: ->
        id = @get('_id')   #this
        if id
            '/projects/'+id
        else
            '/projects'
    
    deleteProject:(id,cb)->
        $.ajax
            type: "delete"
            url: "/projects/"+id
            success: (res) =>
                cb(res) if cb
            error: (err) =>
                console.log('ajax error', err)
                false

    initialize: ->
        console.log 'model init'


