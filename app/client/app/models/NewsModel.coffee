NewsModel = Backbone.Model.extend
    url: ->
        id = this.get('_id')
        if id
            '/news/'+id
        else
            '/news'

    initialize: ->
