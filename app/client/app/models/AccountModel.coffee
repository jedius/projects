AccountModel = Backbone.Model.extend

  initialize: ->
    @data = [
      name: 'Grandpa Joe'
      payments: [
        date: 'May-11'
        amount: 50
      ,
        date: 'Jun-11'
        amount: 40 
      ,
        date: 'Dec-11'
        amount: 30 
      ,
        date: 'May-12'
        amount: 20 
      ,
        date: 'Dec-12'
        amount: 17
      ,
        date: 'Mar-13'
        amount: 50 
      ]
    ,
      name: 'Grandma Tan'
      payments: [
        date: 'May-11'
        amount: 30
      ,
        date: 'Jun-11'
        amount: 40 
      ,
        date: 'Dec-11'
        amount: 20 
      ,
        date: 'May-12'
        amount: 70 
      ,
        date: 'Dec-12'
        amount: 36
      ,
        date: 'Mar-13'
        amount: 74 
      ]
    ]
    total = 0
    for d in @data
      d.subtotal = 0
      for p in d.payments
        d.subtotal += p.amount
        total += p.amount
    @set 'data', @data
    @set 'total', total