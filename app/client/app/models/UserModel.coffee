UserModel = Backbone.Model.extend
	initialize: ->
        console.log 'user model init'
    
    login:(data, cb)->
        $.ajax
            type: "POST"
            url: "/login"
            data: data
            success: (res) =>
                console.log 'res', res
                if res.code is 200
                    cb(res) if cb
                else
                    console.log(res.code)
                    $('.alert-message').show()
                    if res.error
                        $('#error-message').text(res.error)
            error: (err) =>
                console.log(err)
                false

    signUp:(data, cb)->
        $.ajax
            type: "POST"
            url: "/signup"
            data: data
            success: (res) =>
                console.log('ajax success', res)
                if res.code is 200
                    console.log(res.code)
                    cb(res) if cb
                else
                    console.log(res.code)
                    $('.alert-message-reg').show()
                    if res.error
                        $('#error-message-reg').text(res.error)
            error: (err) =>
                console.log('ajax error', err)
                false

