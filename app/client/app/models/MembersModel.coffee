Member = Backbone.Model.extend(
  url: ->
    id = @get("_id")
    if id
      "/members/" + @get("_id")
    else
      "/members"

  idAttribute: "_id"
  initialize: ->
    console.log 'init member'
  
  #this.validators = {};
  
  #console.log('value', value);
  #this.validators.name = function (value) {
  #return value.length > 0 ? {isValid: true} : {isValid: false, message: "You must enter a name"};
  #};
  
  #this.validators.grapes = function (value) {
  #return value.length > 0 ? {isValid: true} : {isValid: false, message: "You must enter a grape variety"};
  #};
  
  #this.validators.country = function (value) {
  #return value.length > 0 ? {isValid: true} : {isValid: false, message: "You must enter a country"};
  #};
  validateItem: (key) ->
    (if (@validators[key]) then @validators[key](@get(key)) else isValid: true)

  
  # TODO: Implement Backbone's standard validate() method instead.
  validateAll: ->
    messages = {}
    for key of @validators
      if @validators.hasOwnProperty(key)
        check = @validators[key](@get(key))
        messages[key] = check.message  if check.isValid is false
    #(if _.size (messages) > 0 then
      #isValid: false
      #messages: messages
     #else isValid: true)
)
