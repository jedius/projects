window.ProjectCollection = Backbone.Collection.extend

    models: ProjectModel
    
    url: '/projects'

    initialize: ->
    	console.log 'initialize collections'
	
	getCount: (cb) ->
		console.log 'getCount in work' 
		$.ajax
		  url: "/projectsCount"
		  type: "GET"
		  error: (err) ->
		    console.log 'error', err
		  success: (body, message, xhr) ->
		    console.log 'body, message, xhr', body, message, xhr
		    console.log "res member get count", body
		    console.log xhr.statusCode
		    switch xhr.status
		      when 200
		        cb body.count
		      when 400
		        cb body.error
		      else
		        console.log "body"
