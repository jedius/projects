window.MemberCollection = Backbone.Collection.extend(
  models: window.Member
  url: "/memberlist"
  
  getCount: (cb) ->
    console.log 'getCount in work' 
    $.ajax
      url: "/membersCount"
      type: "GET"
      error: (err) ->
        console.log 'error', err
      success: (body, message, xhr) ->
        console.log 'body, message, xhr', body, message, xhr
        console.log "res member get count", body
        console.log xhr.statusCode
        switch xhr.status
          when 200
            cb body.count
          when 400
            cb body.error
          else
            console.log "body"

)
